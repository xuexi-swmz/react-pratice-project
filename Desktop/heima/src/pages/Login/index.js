import React from 'react'
import './index.scss'
import { Button, Card,  Form, Input } from 'antd'

export const Login = () => {
  const onFinish =(values)=>{
    console.log(values)
  }
  return (
    <div className='login'>
      <Card className='login-container'> 
        <h1 className='login-text'>登录</h1>
        <Form validateTrigger="onBlur" onFinish={onFinish}>
          <Form.Item
            name="mobile"
            rules={[
              {
                required:true,
                message:'请输入手机号'
              },
              {
                pattern:/^1[3-9]\d{9}$/,
                message:'请输入正确的手机号'
              }
            ]}
          >
            <Input size='large' placeholder='请输入手机号' />
          </Form.Item>
          <Form.Item
            name="code"
            rules={[
              {
                required:true,
                message:'请输入验证码'
              }
            ]}
          >
            <Input size='large' placeholder='请输入验证码' />
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit' size='large' block>
              登录
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}
